package com.afpa.Hello.repository;

import com.afpa.Hello.enums.TypeGenre;
import com.afpa.Hello.enums.TypeHello;
import com.afpa.Hello.model.HelloModele;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface HelloRepository extends JpaRepository<HelloModele, Long> {

    List<HelloModele> findByTypeHello(TypeHello type);
    List<HelloModele> findByTypeGenre(TypeGenre type);

}