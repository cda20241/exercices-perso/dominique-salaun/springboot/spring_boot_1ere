package com.afpa.Hello.controller;

import com.afpa.Hello.dto.HelloDto;
import com.afpa.Hello.enums.TypeGenre;
import com.afpa.Hello.enums.TypeHello;
import com.afpa.Hello.model.HelloModele;
import com.afpa.Hello.repository.HelloRepository;
import com.afpa.Hello.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloController {

    @Autowired
    private final HelloRepository helloRepository;
    @Autowired
    private final HelloService helloService;



    @GetMapping("/dto/{id}")
    public HelloDto getHelloDto(@PathVariable Long id) {
        Optional<HelloModele> helloModele = helloRepository.findById(id);

        HelloDto hellodto = new HelloDto();
        hellodto.mapper(helloModele.get());
        return hellodto;
    }





    @GetMapping("/eleve")
    public String hello(@RequestParam(value = "nom", defaultValue = "TonNomIci") String nom,
                        @RequestParam(value = "prenom", defaultValue = "TonPrenomIci") String prenom) {
        return String.format("Bonjour %s !", nom + " " + prenom);

    }

    @GetMapping("/all")
    public List<HelloModele> getAllHello() {
        return helloRepository.findAll();
    }

    @GetMapping("/{id}")
    public HelloModele getHello(@PathVariable Long id) {
        Optional<HelloModele> joueur = helloRepository.findById(id);
        return joueur.orElse(null);
    }

    // Récupérer toutes les hellos





    @PostMapping("")
    public ResponseEntity<?> addHello(@RequestBody HelloModele hello) {
        hello.setId(null);
        HelloModele savedHello = helloRepository.save(hello);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedHello);
    }


    @PostMapping("/addHellos")
    public ResponseEntity<List<?>> addHellos(@RequestBody List<HelloModele> hellos) {
        List<HelloModele> savedHellos = helloRepository.saveAll(hellos);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedHellos);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateHello(@PathVariable Long id, @RequestBody HelloModele hello) {
        if (helloRepository.existsById(id)) {
            hello.setId(id);
            HelloModele updatedHello = helloRepository.save(hello);
            return ResponseEntity.ok(updatedHello);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("L'ID spécifié n'existe pas");
        }
    }


    // Supprimer une personne par son ID
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteHello(@PathVariable Long id) {
        return helloService.helloDelete(id);
    }


    @GetMapping("/byType/{type}")
    public ResponseEntity<List<HelloModele>> getHelloByType(@PathVariable String type) {
        TypeHello typeHello = TypeHello.valueOf(type.toUpperCase());
        List<HelloModele> helloByType = helloRepository.findByTypeHello(typeHello);
        return ResponseEntity.ok(helloByType);
    }

    @GetMapping("/bygenre/{type}")
    public ResponseEntity<List<HelloModele>> getGenreByType(@PathVariable String type) {
        TypeGenre typeGenre = TypeGenre.valueOf(type.toUpperCase());
        List<HelloModele> genreByType = helloRepository.findByTypeGenre(typeGenre);
        return ResponseEntity.ok(genreByType);
    }





}