package com.afpa.Hello.service;

import com.afpa.Hello.model.HelloModele;
import com.afpa.Hello.repository.HelloRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@RequiredArgsConstructor
public class HelloService {

    private final HelloRepository helloRepository;

    public ResponseEntity<String> helloDelete(Long id) {
        // Recherche de personne par son identifiant
        Optional<HelloModele> helloModele = helloRepository.findById(id);

        // Vérifie si le personne existe
        if (helloModele.isPresent()) {
            // Supprime le personne
            helloRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("Message Service : Le Hello est supprimée", HttpStatus.OK);
        } else {
            // Personne non trouvée, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Message Service : Le Hello non trouvée", HttpStatus.NOT_FOUND);
        }
    }
}
