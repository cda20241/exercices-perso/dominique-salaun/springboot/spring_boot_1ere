package com.afpa.Hello.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TypeHello {

    /**
     * Représente un Bonjour au language de tout les jours.
     */

    COURANT ("Courant"),

    /**
     * Représente un Bonjour au language Famillial.
     */
    FAMILLIER ("Famillier"),

    /**
     * Représente un Bonjour au language Courtoie.
     */
    SOUTENU ("Soutenu");



    private final String description;
}
