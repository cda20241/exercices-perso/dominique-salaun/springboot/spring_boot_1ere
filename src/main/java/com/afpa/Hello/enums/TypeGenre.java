package com.afpa.Hello.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TypeGenre {

    /**
     * Représente un Bonjour au language de tout les jours.
     */

    HOMME ("Née Homme"),

    /**
     * Représente un Bonjour au language Famillial.
     */
    FEMME ("Née Femme"),

    /**
     * Représente un Bonjour au language Courtoie.
     */
    NON_SPECIFIER ("Détaille Personnel Privé");



    private final String description;
}
