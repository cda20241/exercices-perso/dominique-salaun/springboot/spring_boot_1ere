package com.afpa.Hello.dto;

import com.afpa.Hello.enums.TypeGenre;
import com.afpa.Hello.enums.TypeHello;
import com.afpa.Hello.model.HelloModele;
import jakarta.validation.constraints.Null;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

@Data
@RequiredArgsConstructor
public class BaseHelloDto {

    private Long id;
    private String nom;
    private String prenom;
    private Integer age;
    private TypeHello typeHello;
    private TypeGenre typeGenre ;
    private String password;

    public void mapper(HelloModele helloModele) {
        this.id = helloModele.getId();
        this.nom = helloModele.getNom();
        this.prenom = helloModele.getPrenom();
        this.age = helloModele.getAge();
        this.password = helloModele.getPassword();
        this.typeHello = helloModele.getTypeHello();
        this.typeGenre = helloModele.getTypeGenre();
    }

}
