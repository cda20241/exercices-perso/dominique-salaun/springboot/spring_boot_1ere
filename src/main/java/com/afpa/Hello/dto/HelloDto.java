package com.afpa.Hello.dto;

import com.afpa.Hello.model.HelloModele;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class HelloDto extends BaseHelloDto{

    private BaseHelloDto baseHelloDto;
@Override
    public void mapper(HelloModele helloModele) {
        super.mapper(helloModele);
        this.baseHelloDto = new BaseHelloDto();
        this.baseHelloDto.mapper(helloModele);

    }


}
