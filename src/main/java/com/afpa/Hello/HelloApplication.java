package com.afpa.Hello;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootApplication
public class HelloApplication {


	/**
	 * Point d'entrée de l'application.
	 *
	 * @param args Les arguments de la ligne de commande (non utilisés dans cet exemple).
	 */
	public static void main(String[] args) { SpringApplication.run(HelloApplication.class, args);

		// Spécifiez l'URL Swagger
		String swaggerUrl = "http://localhost:8080/swagger-ui/index.html";
		// Affiche un message dans le terminal avec le lien vers Swagger
		System.out.println("Ouvrez l'URL Swagger dans votre navigateur : " + swaggerUrl);
	}





}
