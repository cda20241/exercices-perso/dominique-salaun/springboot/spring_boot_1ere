package com.afpa.Hello.model;

import com.afpa.Hello.enums.TypeGenre;
import com.afpa.Hello.enums.TypeHello;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor//Genere tout les constructeurs
@Data // genere tout les @Getters et @Setters
@Entity(name = "Hello")// Nom donné a la table SQL

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class HelloModele {

    @Id // cle primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY)// Auto-incrementation de la colonne Id de la table SQL
    @Column(name = "Id_Hello")// Nom donné a la colonne SQL
    private Long id;

    @Column(name = "Nom")// Nom donné a la colonne SQL
    private String nom;

    @Column(name = "Prenom")// Nom donné a la colonne SQL
    private String prenom;

    @Column(name = "Age")// Nom donné a la colonne SQL
    private Integer age;

    @Column(name = "Password")// Nom donné a la colonne SQL
    private String password;

    @Column(name = "typeHello")// Nom donné a la colonne SQL
    private TypeHello typeHello;

    @Column(name = "TypeGenre")// Nom donné a la colonne SQL
    private TypeGenre typeGenre;

}
